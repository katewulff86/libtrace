/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_HEAP_H
#define TRACE_HEAP_H

#include "object.h"
#include "hash_table.h"

__EXTERN_C_BEGIN_DECLS

void trace_heap_init();
void trace_heap_kill();
object *trace_heap_lookup(void const *);
void trace_heap_add_object(object *);
void trace_heap_filter(filter_pred, void *);
size_t trace_heap_size();

__EXTERN_C_END_DECLS

#endif

