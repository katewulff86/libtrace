/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_STATIC_H
#define TRACE_STATIC_H

#include "common.h"
#include "object.h"

__EXTERN_C_BEGIN_DECLS

void trace_static_init();
void trace_static_kill();

object *trace_static_lookup(void const *);
void trace_static_foreach(void (*)(object *, void *), void *);

__EXTERN_C_END_DECLS

#endif

