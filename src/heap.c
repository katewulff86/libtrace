/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "heap.h"

typedef struct filter_aux_env {
    filter_pred pred;
    void *pred_env;
} filter_aux_env;

void trace_heap_filter_aux(object *, filter_aux_env *);

static table *heap;
static size_t heap_size = 0;

void
trace_heap_init()
{
    heap = trace_hash_table_make();
    heap_size = 0;
}

void
trace_heap_kill()
{
    trace_hash_table_burn(heap, NULL);
    heap_size = 0;
}

object *
trace_heap_lookup(void const *data)
{
    return trace_hash_table_lookup(heap, data);
}

void
trace_heap_add_object(object *obj)
{
    trace_hash_table_insert(heap, obj, free);
    heap_size += obj->meta->size;
}

void
trace_heap_filter(filter_pred pred, void *pred_env)
{
    filter_aux_env env  = {
        .pred = pred,
        .pred_env = pred_env,
    };

    trace_hash_table_foreach(heap, (foreach_func) trace_heap_filter_aux, &env);
}

void
trace_heap_filter_aux(object *obj, filter_aux_env *env)
{
    if (!env->pred(obj, env->pred_env)) {
        heap_size -= obj->meta->size;
        trace_hash_table_remove(heap, obj->data, (burn_func) trace_object_burn);
    }
}

size_t
trace_heap_size()
{
    return heap_size;
}

