/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdlib.h>

#include "allocator.h"
#include "arena.h"
#include "object.h"
#include "thread.h"

void *
trace_make(trace_meta const *meta)
{
    TRACE_SET_STACK_POSITION();
    void *data = calloc(meta->size, 1);
    object *obj = trace_object_make(data, meta);
    trace_arena_push_object(trace_current_thread->current_arena, obj);

    return obj->data;
}

