/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <threads.h>

#include "arena.h"
#include "collector.h"
#include "heap.h"
#include "static.h"
#include "thread_table.h"

static bool trace_started = false;
static thread_local bool trace_thread_started = false;

void
trace_init()
{
    if (trace_started)
        return;
    trace_started = true;
    trace_static_init();
    trace_heap_init();
    trace_thread_table_init();
    trace_collector_init();
    trace_add_static_meta(&trace_meta_meta);
}

void
trace_kill()
{
    if (!trace_started)
        return;
    trace_collector_kill();
    trace_thread_table_kill();
    trace_heap_kill();
    trace_static_kill();
    trace_started = false;
}

void
trace_init_thread()
{
    if (trace_thread_started)
        return;
    trace_thread_started = true;
    trace_current_thread = trace_thread_make((void *) thrd_current(),
                                             trace_current_stack_position,
                                             &trace_current_stack_position);
    trace_thread_table_add_thread(trace_current_thread);
}

void
trace_kill_thread()
{
    if (!trace_thread_started)
        return;
    trace_thread_burn(trace_current_thread);
    trace_thread_started = false;
}

