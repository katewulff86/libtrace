/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_LIST_H
#define TRACE_LIST_H

#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>

#include "common.h"

typedef struct list list;
typedef struct list_element list_element;

struct list {
    _Atomic (list_element *) head;
};

struct list_element {
    list_element *next;
    void *data;
};

__EXTERN_C_BEGIN_DECLS

void *trace_list_make();
void trace_list_burn(list *, burn_func);
void trace_list_foreach(list *, foreach_func, void *);
void trace_list_push(list *, void *);
void *trace_list_pop(list *);

__EXTERN_C_END_DECLS

#endif

