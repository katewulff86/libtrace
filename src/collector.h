/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_COLLECTOR_H
#define TRACE_COLLECTOR_H

#include "common.h"

__EXTERN_C_BEGIN_DECLS

void trace_collector_init();
void trace_collector_kill();

__EXTERN_C_END_DECLS

#endif

