/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdlib.h>
#include "list.h"


void *
trace_list_make()
{
    list *lst = malloc(sizeof(*lst));
    lst->head = NULL;
    
    return lst;
}

void
trace_list_burn(list *lst, burn_func burn)
{
    void *data;
    while ((data = trace_list_pop(lst)) != NULL) {
        if (burn != NULL)
            burn(data);
    }

    free(lst);
}

void
trace_list_foreach(list *lst, foreach_func func, void *func_env)
{
    list_element *head = lst->head;
    while (head != NULL) {
        func(head->data, func_env);
        head = head->next;
    }
}

void
trace_list_push(list *lst, void *data)
{
    list_element *element = malloc(sizeof(*element));
    element->data = data;
    while (true) {
        list_element *head = lst->head;
        element->next = head;
        if (atomic_compare_exchange_weak(&lst->head, &head, element))
            break;
    }
}

void *
trace_list_pop(list *lst)
{
    list_element *head;
    while (true) {
        head = lst->head;
        if (head == NULL)
            return NULL;
        if (atomic_compare_exchange_weak(&lst->head, &head, head->next)) {
            break;
        }
    }

    void *data = head->data;
    free(head);

    return data;
}

