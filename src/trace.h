/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_H
#define TRACE_H

#include <stdbool.h>
#include <stdint.h>
#include <threads.h>

#ifndef __EXTERN_C_BEGIN_DECLS
#ifdef __cplusplus
#define __EXTERN_C_BEGIN_DECLS extern "c" {
#define __EXTERN_C_END_DECLS }
#else
#define __EXTERN_C_BEGIN_DECLS
#define __EXTERN_C_END_DECLS
#endif
#endif

#define TRACE_SET_STACK_POSITION() \
    do { \
        void *__trace_stack_position; \
        trace_current_stack_position = (uintptr_t) &__trace_stack_position; \
    } while (0)

typedef struct trace_meta trace_meta;
typedef void (*trace_kill_func)(void *, void *);
typedef void (*trace_free_func)(void *, void *);
typedef void (*trace_foreach_func)(void *, void *);
typedef void (*trace_children_func)(void *, trace_foreach_func, void *,
                                    void *);
typedef void (*trace_thunk_func)(void *);

struct trace_meta {
    size_t size;
    trace_children_func children;
    void *children_env;
    trace_kill_func kill;
    void *kill_env;
    trace_free_func free;
    void *free_env;
};

__EXTERN_C_BEGIN_DECLS

extern thread_local uintptr_t trace_current_stack_position;

void trace_init();
void trace_kill();
void trace_init_thread();
void trace_kill_thread();

void *trace_make(trace_meta const *);
void trace_add_static_object(void *, trace_meta const *);
void trace_add_static_meta(trace_meta const *);

extern trace_meta const trace_meta_meta;

__EXTERN_C_END_DECLS

#endif

