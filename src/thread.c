/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdlib.h>

#include "hash_table.h"
#include "thread.h"
#include "thread_table.h"

thread_local thread *trace_current_thread;
thread_local uintptr_t trace_current_stack_position;

thread *
trace_thread_make(void *thread_id, uintptr_t stack_base,
                  uintptr_t *current_stack_position)
{
    thread *thrd = malloc(sizeof(*thrd));
    thrd->thread_id = thread_id;
    thrd->current_arena = trace_arena_make();
    thrd->temp_arena = trace_arena_make();
    thrd->stack_base = stack_base;
    thrd->current_stack_position = current_stack_position;
    thrd->final = false;

    return thrd;
}

void
trace_thread_burn(thread *thrd)
{
    *thrd->current_stack_position = thrd->stack_base;
    thrd->final = true;
}

void
trace_thread_swap_arenas(thread *thrd)
{
    arena *s = thrd->current_arena;
    thrd->current_arena = thrd->temp_arena;
    thrd->temp_arena = s;
}

void
trace_thread_stack_foreach(thread *thrd, foreach_func func, void *func_env)
{
    uintptr_t current_stack_position = *thrd->current_stack_position;
    void **stack;
    size_t stack_length;
    if (thrd->stack_base > *thrd->current_stack_position) {
        stack_length = thrd->stack_base - current_stack_position;
        stack = (void **) current_stack_position;
    } else {
        stack_length = current_stack_position - thrd->stack_base;
        stack = (void **) thrd->stack_base;
    }

    stack_length /= sizeof(void *);
    for (size_t i = 0; i < stack_length; ++i)
        func(stack[i], func_env);
}

