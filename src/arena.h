/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_ARENA_H
#define TRACE_ARENA_H

#include <threads.h>

#include "object.h"
#include "list.h"

typedef struct arena arena;

struct arena {
    list *objects;
    atomic_size_t allocated_size;
};

__EXTERN_C_BEGIN_DECLS

arena *trace_arena_make();
void trace_arena_kill(arena *);
void trace_arena_free(arena *);
void trace_arena_burn(arena *);
void trace_arena_objects_foreach(arena *, void (*)(object *, void *), void *);
void trace_arena_move_objects_to_heap(arena *);
void trace_arena_push_object(arena *, object *);
object *trace_arena_pop_object(arena *);

__EXTERN_C_END_DECLS

#endif

