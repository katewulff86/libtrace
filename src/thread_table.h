/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_THREAD_TABLE_H
#define TRACE_THREAD_TABLE_H

#include "hash_table.h"
#include "thread.h"

__EXTERN_C_BEGIN_DECLS

void trace_thread_table_init();
void trace_thread_table_kill();

void trace_thread_table_add_thread(thread *);
void trace_thread_table_remove_thread(thread *);

void trace_thread_table_foreach(void (*)(thread *, void *), void *);

__EXTERN_C_END_DECLS

#endif

