/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "thread_table.h"

static table *trace_thread_table;

void
trace_thread_table_init()
{
    trace_thread_table = trace_hash_table_make();
}

void
trace_thread_table_kill()
{
    trace_hash_table_burn(trace_thread_table, (burn_func) trace_thread_burn);
}

void
trace_thread_table_add_thread(thread *thrd)
{
    trace_hash_table_insert(trace_thread_table, thrd,
                            (burn_func) trace_thread_burn);
}

void
trace_thread_table_remove_thread(thread *thrd)
{
    trace_hash_table_remove(trace_thread_table, thrd->thread_id,
                            free);
}

void
trace_thread_table_foreach(void (*func)(thread *, void *), void *func_env)
{
    trace_hash_table_foreach(trace_thread_table, (foreach_func) func, func_env);
}

