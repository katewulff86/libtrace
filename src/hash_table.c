/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash_table.h"

#define TOMBSTONE ((void *) -1)

static uint64_t hash(void const *);
static bool bucket_empty_p(bucket const *);
static bucket **search(table const *, void const *);
static bucket **find_empty(table const *, void const *);
static void shrink_table(table *);
static void grow_table(table *);
static void resize_table(table *, size_t);

table *
trace_hash_table_make()
{
    table *table_ = malloc(sizeof(*table_));
    table_->buckets = calloc(sizeof(*table_->buckets), 1);
    table_->size = 0;
    table_->capacity = 1;

    return table_;
}

void
trace_hash_table_kill(table *table_, burn_func burn)
{
    for (size_t index = 0; index < table_->capacity; ++index) {
        bucket *bucket_ = table_->buckets[index];
        if (!bucket_empty_p(bucket_)) {
            if (burn != NULL)
                burn(bucket_);
        }
    }

    free(table_->buckets);
}

void
trace_hash_table_free(table *table_)
{
    free(table_);
}

void
trace_hash_table_burn(table *table_, burn_func burn)
{
    trace_hash_table_kill(table_, burn);
    trace_hash_table_free(table_);
}

void
trace_hash_table_insert(table *table_, void *b, burn_func burn)
{
    mtx_lock(&table_->lock);
    bucket *bucket_ = b;
    void const *key = bucket_->key;

    /* this object may already exist in the table. look for it! */
    bucket **existing_bucket = search(table_, key);
    if (*existing_bucket == NULL) {
        /* no luck? find an empty spot */
        existing_bucket = find_empty(table_, key);
    }

    bucket *bucket_in_slot = *existing_bucket;
    if (bucket_in_slot != bucket_) {
        *existing_bucket = bucket_;
        if (bucket_empty_p(bucket_in_slot)) {
            table_->size += 1;
            grow_table(table_);
        } else if (burn != NULL) {
            burn(bucket_in_slot);
        }
    }

    mtx_unlock(&table_->lock);
}

void
trace_hash_table_remove(table *table_, void const *key, burn_func burn)
{
    mtx_lock(&table_->lock);
    bucket **bucket_ = search(table_, key);
    if (!bucket_empty_p(*bucket_)) {
        if (burn != NULL)
            burn(*bucket_);
        *bucket_ = TOMBSTONE;
        table_->size -= 1;
        shrink_table(table_);
    }
    mtx_unlock(&table_->lock);
}

void *
trace_hash_table_lookup(table *table_, void const *key)
{
    mtx_lock(&table_->lock);
    bucket **result = search(table_, key);
    mtx_unlock(&table_->lock);

    return *result;
}

void
grow_table(table *table_)
{
    if (table_->size * 4 <= table_->capacity * 3)
        return;

    resize_table(table_, table_->capacity * 2);
}

void
shrink_table(table *table_)
{
    if (table_->size * 4 >= table_->capacity && table_->capacity > 1)
        return;

    resize_table(table_, table_->capacity / 2);
}

void
resize_table(table *table_, size_t new_capacity)
{
    size_t capacity = table_->capacity;
    bucket **buckets = table_->buckets;

    table_->capacity = new_capacity;
    table_->buckets = calloc(sizeof(*table_->buckets), table_->capacity);
    for (size_t i = 0; i < capacity; ++i) {
        bucket *bucket_ = buckets[i];
        if (!bucket_empty_p(bucket_)) {
            bucket **empty = find_empty(table_, bucket_->key);
            *empty = bucket_;
        }
    }

    free(buckets);
}

void
trace_hash_table_foreach(table *table_, foreach_func func, void *func_env)
{
    mtx_lock(&table_->lock);
    bucket **buckets = malloc(sizeof(*buckets) * table_->capacity);
    memcpy(buckets, table_->buckets, sizeof(*buckets) * table_->capacity);
    size_t capacity = table_->capacity;
    mtx_unlock(&table_->lock);

    for (size_t index = 0; index < capacity; ++index) {
        bucket *bucket_ = buckets[index];
        if (!bucket_empty_p(bucket_))
            func(bucket_, func_env);
    }

    free(buckets);
}


uint64_t
hash(void const *o)
{
    uint64_t h = (uint64_t) o;
    h ^= h << 13;
    h ^= h >> 7;
    h ^= h << 17;

    return h;
}

bool
bucket_empty_p(bucket const *bucket_)
{
    return bucket_ == NULL || bucket_ == TOMBSTONE;
}

bucket **
search(table const *table_, void const *key)
{
    uint64_t hash_ = hash(key);
    uint64_t index = hash_ % table_->capacity;
    uint64_t skip = 0;
    while (table_->buckets[index] != NULL) {
        if (table_->buckets[index] != TOMBSTONE &&
            table_->buckets[index]->key == key)
            break;
        ++skip;
        index = (index + skip) % table_->capacity;
    }

    return &table_->buckets[index];
}

bucket **
find_empty(table const *table_, void const *key)
{
    uint64_t hash_ = hash(key);
    uint64_t index = hash_ % table_->capacity;
    uint64_t skip = 0;
    while (!bucket_empty_p(table_->buckets[index])) {
        ++skip;
        index = (index + skip) % table_->capacity;
    }

    return &table_->buckets[index];
}

