/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_RELEASE_H
#define TRACE_RELEASE_H

#include "arena.h"
#include "trace_heap.h"

__EXTERN_C_BEGIN_DECLS

void trace_release_unreachable(table *);

__EXTERN_C_END_DECLS

#endif

