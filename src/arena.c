/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "arena.h"
#include "heap.h"

arena *
trace_arena_make()
{
    arena *arena_ = malloc(sizeof(*arena_));
    arena_->objects = trace_list_make();
    arena_->allocated_size = 0;

    return arena_;
}

void
trace_arena_kill(arena *arena_)
{
    trace_list_burn(arena_->objects, (burn_func) trace_object_burn);
}

void
trace_arena_free(arena *arena_)
{
    free(arena_);
}

void
trace_arena_burn(arena *arena_)
{
    trace_arena_kill(arena_);
    trace_arena_free(arena_);
}

void
trace_arena_objects_foreach(arena *arena_, void (*func)(object *, void *),
                            void *func_env)
{
    trace_list_foreach(arena_->objects, (foreach_func) func, func_env);
}

void
trace_arena_move_objects_to_heap(arena *arena_)
{
    object *obj;
    while ((obj = trace_list_pop(arena_->objects)) != NULL)
        trace_heap_add_object(obj);
}

void
trace_arena_push_object(arena *arena_, object *obj)
{
    trace_list_push(arena_->objects, obj);
    arena_->allocated_size += obj->meta->size;
}

object *
trace_arena_pop_object(arena *arena_)
{
    object *obj = trace_list_pop(arena_->objects);
    if (obj != NULL)
        arena_->allocated_size -= obj->meta->size;

    return obj;
}

