/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "collector.h"
#include "heap.h"
#include "release.h"
#include "thread.h"
#include "thread_table.h"
#include "trace_heap.h"

static thrd_t collector_thread;
static bool collector_should_exit = false;
static volatile bool force_collection = false;

static int collect(void *);
static bool ready_to_run();
static void move_objects_to_heap();
static void add_allocated_size(thread *, size_t *);
static void move_thread_objects_to_heap(thread *, void *);

void
trace_collector_init()
{
    collector_should_exit = false;
    thrd_create(&collector_thread, collect, NULL);
}

void
trace_collector_kill()
{
    force_collection = true;
    while (force_collection)
        thrd_yield();
    collector_should_exit = true;
    thrd_join(collector_thread, NULL);
}

int
collect(void *e)
{
    (void) e;

    while (!collector_should_exit) {
        if (ready_to_run()) {
            move_objects_to_heap();
            table *reachables = trace_heap();
            trace_release_unreachable(reachables);
        }

        force_collection = false;
        thrd_yield();
    }

    return 0;
}

bool
ready_to_run()
{
    if (force_collection)
        return true;
    size_t allocated_size = 0;
    trace_thread_table_foreach((void (*)(thread *, void *)) add_allocated_size,
                               &allocated_size);

    return allocated_size >= trace_heap_size();
}

void
add_allocated_size(thread *thrd, size_t *allocated_size)
{
    *allocated_size += thrd->current_arena->allocated_size;
}

void
move_objects_to_heap()
{
    trace_thread_table_foreach(move_thread_objects_to_heap, NULL);
}

void
move_thread_objects_to_heap(thread *thrd, void *e)
{
    (void) e;
    trace_thread_swap_arenas(thrd);
    trace_arena_move_objects_to_heap(thrd->temp_arena);
    if (thrd->final)
        trace_thread_table_remove_thread(thrd);
}

