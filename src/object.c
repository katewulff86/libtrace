/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "heap.h"
#include "object.h"
#include "static.h"

object *
trace_object_zone()
{
    return malloc(sizeof(object));
}

object *
trace_object_init(object *obj, void *data, trace_meta const *meta)
{
    obj->data = data;
    obj->meta = meta;

    return obj;
}

object *
trace_object_make(void *data, trace_meta const *meta)
{
    object *obj = trace_object_zone();

    return trace_object_init(obj, data, meta);
}

void
trace_object_kill(object *obj)
{
    if (obj == NULL)
        return;

    trace_meta const *meta = obj->meta;

    trace_kill_func kill = meta->kill;
    if (kill != NULL)
        kill(obj->data, meta->kill_env);

    trace_free_func free_ = meta->free;
    if (free_ != NULL)
        free_(obj->data, meta->free_env);
    else
        free(obj->data);
}

void
trace_object_free(object *obj) {
    free(obj);
}

void
trace_object_burn(object *obj) {
    trace_object_kill(obj);
    trace_object_free(obj);
}

object *
trace_lookup_object(void const *data)
{
    if (data == NULL)
        return NULL;

    object *obj = trace_static_lookup(data);
    if (obj != NULL)
        return obj;
    obj = trace_heap_lookup(data);
    if (obj != NULL)
        return obj;

    return NULL;
}

