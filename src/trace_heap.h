/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_TRACE_HEAP_H
#define TRACE_TRACE_HEAP_H

#include <stdbool.h>

#include "hash_table.h"
#include "object.h"
#include "trace.h"

typedef struct reachable reachable;

struct reachable {
    void *data;
    trace_meta const *meta;
    bool visited;
};

__EXTERN_C_BEGIN_DECLS

table *trace_heap();

__EXTERN_C_END_DECLS

#endif

