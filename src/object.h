/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_OBJECT_H
#define TRACE_OBJECT_H

#include <stdint.h>

#include "trace.h"

typedef struct object object;

struct object {
    void *data;
    trace_meta const *meta;
};

__EXTERN_C_BEGIN_DECLS

/* object life cycle
 * `zone` allocate memory, but don't initialise
 * `init` initialise already allocated memory
 * `make` convenience function, combine `zone` and `init`
 * `kill` deinitialise/finalise
 * `free` release memory
 * `burn` convenience function, combine `kill` and `free`
 */

object *trace_object_zone();
object *trace_object_init(object *, void *, trace_meta const *);
object *trace_object_make(void *, trace_meta const *);
void trace_object_kill(object *);
void trace_object_free(object *);
void trace_object_burn(object *);

object *trace_lookup_object(void const *);

__EXTERN_C_END_DECLS

#endif

