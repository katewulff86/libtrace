/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_HASH_TABLE_H
#define TRACE_HASH_TABLE_H

#include <stdatomic.h>
#include <threads.h>

#include "common.h"

typedef struct bucket bucket;
typedef struct table table;

struct bucket {
    void *key;
};

struct table {
    bucket **buckets;
    atomic_size_t size;
    atomic_size_t capacity;
    mtx_t lock;
};

__EXTERN_C_BEGIN_DECLS

table *trace_hash_table_make();
void trace_hash_table_kill(table *, burn_func);
void trace_hash_table_free(table *);
void trace_hash_table_burn(table *, burn_func);
void trace_hash_table_insert(table *, void *, burn_func);
void trace_hash_table_remove(table *, void const *, burn_func);
void *trace_hash_table_lookup(table *, void const *);
void trace_hash_table_foreach(table *, foreach_func, void *);

__EXTERN_C_END_DECLS

#endif

