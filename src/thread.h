/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_THREAD_H
#define TRACE_THREAD_H

#include <threads.h>

#include "arena.h"
#include "list.h"

typedef struct thread thread;
typedef struct arena_list arena_list;

struct thread {
    void *thread_id;
    arena *current_arena;
    arena *temp_arena;
    uintptr_t stack_base;
    uintptr_t *current_stack_position;
    bool final;
};

extern thread_local thread *trace_current_thread;

__EXTERN_C_BEGIN_DECLS

thread *trace_thread_make(void *, uintptr_t, uintptr_t *);
void trace_thread_burn(thread *);

void trace_thread_swap_arenas(thread *);
void trace_thread_arenas_foreach(thread *, void (*)(arena *, void *), void *);
void trace_thread_stack_foreach(thread *, foreach_func, void *);

__EXTERN_C_END_DECLS

#endif

