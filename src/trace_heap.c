/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "arena.h"
#include "common.h"
#include "heap.h"
#include "object.h"
#include "static.h"
#include "thread.h"
#include "thread_table.h"
#include "trace_heap.h"

static reachable *reachable_make(object *);
static void trace_data(void const *, table *);
static void trace_reachable(reachable *, table *);
static void trace_reachable_aux(void *, table *);
static void trace_object(object *, table *);
static void trace_thread(thread *, table *);
static void trace_arena(arena *, table *);

table *
trace_heap()
{
    table *reachables = trace_hash_table_make();
    trace_static_foreach((void (*)(object *, void *)) trace_object, reachables);
    trace_thread_table_foreach((void (*)(thread *, void *)) trace_thread,
                               reachables);

    return reachables;
}

reachable *
reachable_make(object *obj)
{
    reachable *r = malloc(sizeof(*r));
    r->data = obj->data;
    r->meta = obj->meta;
    r->visited = false;

    return r;
}

void
trace_data(void const *data, table *reachables)
{
    if (data == NULL)
        return;
    object *obj = trace_lookup_object(data);
    trace_object(obj, reachables);
}

void
trace_object(object *obj, table *reachables)
{
    if (obj == NULL)
        return;
    reachable *r =  trace_hash_table_lookup(reachables, obj->data);
    if (r == NULL) {
        r = reachable_make(obj);
        trace_hash_table_insert(reachables, r, free);
    }
    trace_reachable(r, reachables);
}

void
trace_reachable(reachable *r, table *reachables)
{
    if (r->visited)
        return;
    r->visited = true;
    void *data = r->data;
    trace_meta const *meta = r->meta;
    trace_children_func children = meta->children;
    void *children_env = meta->children_env;
    if (children != NULL)
        children(data, (trace_foreach_func) trace_reachable_aux, reachables,
                 children_env);
}

void
trace_reachable_aux(void *data, table *reachables)
{
    if (data == NULL)
        return;
    reachable *r = trace_hash_table_lookup(reachables, data);
    if (r == NULL) {
        object *obj = trace_lookup_object(data);
        if (obj == NULL)
            return;
        r = reachable_make(obj);
        trace_hash_table_insert(reachables, r, free);
    }
    trace_reachable(r, reachables);
}

void
trace_thread(thread *thrd, table *reachables)
{
    trace_arena(thrd->current_arena, reachables);
    trace_thread_stack_foreach(thrd, (foreach_func) trace_data, reachables);
}

void
trace_arena(arena *arena_, table *reachables)
{
    trace_arena_objects_foreach(arena_,
                                (void (*)(object *, void *)) trace_object,
                                reachables);
}

