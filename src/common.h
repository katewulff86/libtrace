/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef TRACE_COMMON_H
#define TRACE_COMMON_H

#include <stdbool.h>

#ifndef __EXTERN_C_BEGIN_DECLS
#ifdef __cplusplus
#define __EXTERN_C_BEGIN_DECLS extern "c" {
#define __EXTERN_C_END_DECLS }
#else
#define __EXTERN_C_BEGIN_DECLS
#define __EXTERN_C_END_DECLS
#endif
#endif

typedef void (*free_func)(void *);
typedef void (*kill_func)(void *);
typedef void (*burn_func)(void *);
typedef void (*foreach_func)(void *, void *);
typedef bool (*filter_pred)(void *, void *);

#endif

