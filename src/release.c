/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "heap.h"
#include "release.h"

static bool object_reachable_p(object *, table *);

void
trace_release_unreachable(table *reachables)
{
    trace_heap_filter((filter_pred) object_reachable_p, reachables);
}

bool
object_reachable_p(object *obj, table *reachables)
{
    return trace_hash_table_lookup(reachables, obj->data) != NULL;
}

