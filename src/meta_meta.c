/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include "trace.h"

static void meta_children(trace_meta *, trace_foreach_func, void *, void *);

trace_meta const trace_meta_meta  = {
    .size = sizeof(trace_meta),
    .children = (trace_children_func) meta_children,
    .children_env = NULL,
    .kill = NULL,
    .kill_env = NULL,
    .free = NULL,
    .free_env = NULL,
};

void
meta_children(trace_meta *meta, trace_foreach_func func, void *func_env,
              void *e)
{
    (void) e;
    if (meta->children_env != NULL)
        func(meta->children_env, func_env);
    if (meta->kill_env != NULL)
        func(meta->kill_env, func_env);
    if (meta->free_env != NULL)
        func(meta->free_env, func_env);
}

