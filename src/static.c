/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>

#include "static.h"
#include "hash_table.h"

static table *static_table;

void
trace_static_init()
{
    static_table = trace_hash_table_make();
}

void
trace_static_kill()
{
    trace_hash_table_burn(static_table, (kill_func) trace_object_free);
}

void
trace_add_static_object(void *data, trace_meta const *meta)
{
    object *obj = trace_object_make(data, meta);
    trace_hash_table_insert(static_table, obj, free);
}

void
trace_add_static_meta(trace_meta const *meta)
{
    trace_add_static_object((void *) meta, &trace_meta_meta);
}

object *
trace_static_lookup(void const *data)
{
    object *obj = trace_hash_table_lookup(static_table, data);

    return obj;
}

void
trace_static_foreach(void (*func)(object *, void *), void *func_env)
{
    trace_hash_table_foreach(static_table, (foreach_func) func, func_env);
}

