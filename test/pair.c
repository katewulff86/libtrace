#include <stdatomic.h>
#include <stdio.h>
#include <trace.h>
#include "pair.h"

atomic_size_t object_count = 0;

struct pair {
    void *car;
    void *cdr;
};

void
pair_kill(pair *pair, void *env)
{
    (void) pair;
    (void) env;
    --object_count;
}

void
pair_children(pair *pair, trace_foreach_func func, void *func_env, void *env)
{
    (void) env;
    func(pair->car, func_env);
    func(pair->cdr, func_env);
}

trace_meta const pair_meta = {
    .size = sizeof(pair),
    .kill = (trace_kill_func) pair_kill,
    .kill_env = NULL,
    .children = (trace_children_func) pair_children,
    .children_env = NULL,
    .free_env = NULL,
};

pair *
pair_make(void *car, void *cdr)
{
    pair *pair = trace_make(&pair_meta);
    pair->car = car;
    pair->cdr = cdr;
    ++object_count;

    return pair;
}

void *
pair_car(pair *pair)
{
    return pair->car;
}

void *
pair_cdr(pair *pair)
{
    return pair->cdr;
}

void
pair_set_car(pair *pair, void *car)
{
    pair->car = car;
}

void
pair_set_cdr(pair *pair, void *cdr)
{
    pair->cdr = cdr;
}

