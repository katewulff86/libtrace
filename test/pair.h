#ifndef TRACE_TEST_PAIR_H
#define TRACE_TEST_PAIR_H

#include <stdatomic.h>
#include <stddef.h>

#include "trace.h"

typedef struct pair pair;

pair *pair_make(void *, void *);
void *pair_car(pair *);
void *pair_cdr(pair *);
void pair_set_car(pair *, void *);
void pair_set_cdr(pair *, void *);

extern atomic_size_t object_count;
extern trace_meta const pair_meta;

#endif
