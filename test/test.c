#include <stdio.h>
#include <ctest.h>
#include <trace.h>
#include "pair.h"

void *
trace_fixture_setup(void *e)
{
    (void) e;
    TRACE_SET_STACK_POSITION();
    trace_init();
    trace_init_thread();
    trace_add_static_meta(&pair_meta);
    object_count = 0;

    return NULL;
}

void
trace_fixture_teardown(void *e)
{
    (void) e;
    trace_kill_thread();
    trace_kill();
    assert(object_count == 0);
}

test_fixture trace_fixture = {
    .parent = NULL,
    .setup = trace_fixture_setup,
    .teardown = trace_fixture_teardown
};

static void
smoke_test(void *e)
{
    (void) e;
}

static void
simple_test(void *e)
{
    (void) e;
    TRACE_SET_STACK_POSITION();
    pair *p = pair_make(NULL, NULL);
    (void) p;
}

static void
cycle_test(void *e)
{
    (void) e;
    TRACE_SET_STACK_POSITION();
    pair *a = pair_make(NULL, NULL);
    pair *b = pair_make(NULL, a);
    pair_set_cdr(a, b);
}

TEST_CASE_WITH_FIXTURE(smoke_test, &trace_fixture)
TEST_CASE_WITH_FIXTURE(simple_test, &trace_fixture)
TEST_CASE_WITH_FIXTURE(cycle_test, &trace_fixture)
TEST_SUITE(libtrace_test_suite,
           &smoke_test_case,
           &simple_test_case,
           &cycle_test_case)

TEST_MAIN(&libtrace_test_suite)

